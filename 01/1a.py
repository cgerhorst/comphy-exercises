import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

fig = plt.figure()
for i, (n, t, l) in enumerate(zip(["ferro", "antiferro"], ["Ferromagnetic", "Antiferromagnetic"], ["upper right", "lower right"])):
    ax = fig.add_subplot(2, 1, i + 1)
    ax.set_title(t)
    for N in (2, 5, 10):
        θ, E = np.loadtxt("1a-{}-{}.csv".format(n, N), unpack=True)
        ax.plot(θ, E, '-', label="$N = {}$".format(N))
    ax.set_xlim(0, 2 * 3.14)
    ax.set_xlabel(r"$\theta$")
    ax.set_ylabel(r"$E(N, \theta)$")
    ax.legend(loc=l)
fig.savefig("1a.pdf")
