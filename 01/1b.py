import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

fig = plt.figure()
for i, (n, t, l) in enumerate(zip(["ferro", "antiferro"], ["Ferromagnetic", "Antiferromagnetic"], [(0.62, 0.04), (0.62, 0.29)])):
    ax = fig.add_subplot(2, 1, i + 1)
    ax.set_title(t)
    for N in (2, 5, 10):
        θ, T, T_analytic = np.loadtxt("1b-{}-{}.csv".format(n, N), unpack=True)
        ax.plot(θ, T, '-', label="$N = {}$, numeric".format(N))
        ax.plot(θ, T_analytic, '--', label="$N = {}$, analytic".format(N))
    ax.set_xlim(0, 2 * 3.14)
    ax.set_xlabel(r"$\theta$")
    ax.set_ylabel(r"$T(N, \theta)$")
    ax.legend(loc=l)
fig.savefig("1b.pdf")
