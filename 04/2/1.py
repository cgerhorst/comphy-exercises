import matplotlib.pyplot as plt
import numpy as np

t, E = np.loadtxt("1a.csv", usecols=(0, 3), unpack=True)

plt.plot(t, E, label="Gesamtenergie bei $N = 400$")
plt.legend(loc="best")
plt.xlabel("$t$")
plt.ylabel(r"$E$")
plt.savefig("1a.pdf")
plt.clf()

for u in [0.25, 0.5, 1.0]:

    t, x, v = np.loadtxt("1b-{:.2f}.csv".format(u), unpack=True)
    plt.plot(x / np.pi, v, label="$Q = {:.2f}$".format(u))

plt.ylabel(r"$\dot{\theta}$")
plt.xlabel(r"$\theta\, / \, \pi$")
plt.legend(loc='best')
plt.savefig("1b.pdf")
