#include <cmath>
#include <cstdio>
#include <functional>
#include <fstream>

#include "rungekutta.h"

using namespace std;

void a(double A, double w)
{
    auto f = [A, w](double x, double *y, double *yd)
    {
        yd[0] = y[1];
        yd[1] = -y[0] + A * cos(w * x);
    };

    unsigned int N = 200000;
    double x_0 = 0;
    double x_n = 200;
    double h = (x_n - x_0) / N;
    double **ys = new double*[N+1];
    for (size_t i = 0; i <= N; i++)
    {
        ys[i] = new double[2];
    }

    ys[0][0] = 1;
    ys[0][1] = 0;
    int n = 4;

    runge_kutta(f, 2, N, x_0, x_n, ys); 

    FILE *outfile = fopen("3a.csv", "w");
    for (size_t i = 0; i <= N; i++)
    {
        double x = x_0 + h * i;
        int plot = 0;
        if (fabs(x - n * 2 * M_PI) <  h)
        {
            plot = 1;
            n++;
        }
        fprintf(outfile, "%.10f %u %.10f %.10f\n", x, plot, ys[i][0], ys[i][1]);
    }
    fclose(outfile);
    for (size_t i = 0; i <= N; i++)
    {
        delete[] ys[i];
    }
    delete[] ys;
}

void b()
{
    double A = 1.5;
    double w = 2.0 / 3;
    unsigned int N = 10000;
    double x_0 = 0;
    double x_n = 100 * M_PI;
    double **ys = new double*[N+1];
    for (size_t i = 0; i <= N; i++)
    {
        ys[i] = new double[2];
    }
    ys[0][0] = 0;

    int initial_velocities[] = {-5, -3, 0, 1, 2};

    for (size_t j = 0; j < 5; j++)
    {
        ys[0][1] = initial_velocities[j];
        char filename[10];
        sprintf(filename, "3b-%d.csv", initial_velocities[j]);
        FILE *outfile = fopen(filename, "w");

        for (size_t i = 0; i < 500; i++)
        {
            double Q = 0.5 + i * 0.9 / 500;
            auto f = [A, w, Q](double x, double *y, double *yd)
            {
                yd[0] = y[1];
                yd[1] = -sin(y[0]) - y[1] / Q + A * cos(w * x);
            };
            runge_kutta(f, 2, N, x_0, x_n, ys); 
            fprintf(outfile, "%.2f %.10f\n", Q, ys[N-42][1]);
        } 
        fclose(outfile);
    }
    for (size_t i = 0; i <= N; i++)
    {
        delete[] ys[i];
    }
    delete[] ys;
}

int main()
{
    a(0.0, 0.666666);
    b();   
}

