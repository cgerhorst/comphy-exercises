#include <cmath>
#include <cstdio>
#include <functional>
#include <omp.h>

#include "newton.h"

using namespace std;

void ex3a()
{
	auto homolinear = [](double t, double *theta, double *F) {F[0] = - theta[0];};
	size_t N = 200000;
	double t1 = 2 * M_PI * 1000;
	double **rv = new double*[N + 1];
	for (size_t j = 0; j <= N; j++)
	{
		rv[j] = new double[2];
	}
	rv[0][0] = 1;
	rv[0][1] = 0;
	newton(homolinear, N, 1, t1, rv);
	FILE *file = fopen("3a.csv", "w");
	for (size_t j = 0; j < N; j += 200)
	{
		fprintf(file, "%.15f %.15f\n", rv[j][0], rv[j][1]);
	}
	fclose(file);
	for (size_t j = 0; j <= N; j++)
	{
		delete[] rv[j];
	}
	delete[] rv;
}

void ex3b()
{
	double A = 1.5;
	double omega = 2./ 3;

	size_t N = 200000;
	double t1 = 2 * M_PI * 1000;	
	double **rv = new double*[N + 1];
	for (size_t j = 0; j <= N; j++)
	{
		rv[j] = new double[2];
	}
	rv[0][0] = 0;

	double dtheta0[] = {-5, -3, 0, 3, 5};
	#pragma omp parallel for
	for (size_t i = 0; i < 5; i++)
	{
		rv[0][1] = dtheta0[i];
		char name[10];
		sprintf(name, "3b%zu.csv", i);
		FILE *file = fopen(name, "w");
		for (size_t j = 0; j < 1000; j++)
		{
			double Q = 0.5 + 1.4e-3 * j;
			auto inhomonichtlinear = [A, omega, Q](double t, double *theta, double *F) {F[0] = -theta[1] / Q - sin(theta[0]) + A * cos(omega * t);};
			newton(inhomonichtlinear, N, 1, t1, rv);
			fprintf(file, "%.15f %.15f\n", Q, rv[N - 199][1]);
		}
		fclose(file);
	}

	for (size_t i = 0; i <= N; i++)
	{
		delete[] rv[i];
	}
	delete[] rv;
}

int main()
{
	ex3a();
	ex3b();
	return 0;
}
