#include <functional>

#ifndef NEWTON_H
#define NEWTON_H

void rungekutta4(std::function<void(double, double*, double*)> f, int D, int N, double x0, double x1, double **y);

void newton(std::function<void(double, double*, double*)> F, int N, int D, double t1, double **rv);

#endif
