import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

H, m = np.loadtxt('1.csv', unpack=True)

m_ana = lambda x: np.tanh(x)
H_ana = np.linspace(-5, 5, 1000)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(H_ana, m_ana(H_ana), "r-", label='anal.')
ax.plot(H, m, 'b+', label='numerisch')
ax.set_xlim(-5, 5)
ax.set_xlabel("$H$")
ax.set_ylabel("$m$")
ax.legend(loc='upper left')
fig.savefig("1.pdf")
