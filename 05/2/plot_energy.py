#!/usr/bin/env python

from pylab import *
import sys

t, Ekin, Epot, vx, vy = loadtxt(sys.argv[1]).T

plot(t, Ekin, label=r'$E_{\mathrm{kin}}$')
plot(t, Epot, label=r'$E_{\mathrm{pot}}$')
plot(t, Ekin + Epot, label=r'$E_{\mathrm{ges}}$')
xlabel('$t$')
ylabel('$E$')
legend(loc='best')
savefig('{}.pdf'.format(sys.argv[1][:-4]))
clf()

plot(t, sqrt(vx**2+vy**2))
xlabel('$t$')
ylabel(r'$|\vec{v}|$')
savefig('vmean_{}.pdf'.format(sys.argv[1][:-4]))
clf()
