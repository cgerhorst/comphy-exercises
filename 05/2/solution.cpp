#include <vector>
#include <functional>
#include <iostream>
#include <fstream>
#include <tuple>
#include <random>

#include <Eigen/Dense>

#include "verlet.h"

using namespace std;

typedef Eigen::Vector2d V;

template <typename V>
V mean(vector<V> arr)
{
	V sum = V::Zero();
    for(auto el: arr) {
    	sum += el;
    }
    return sum / arr.size();
}

void scale_velocities(vector<V> &vs, double Ekin_needed) {
    double Ekin_found = 0;
    V sum_v = V::Zero();
    for (V &v: vs) {
        Ekin_found += v.squaredNorm() / 2;
    }
    for (V &v: vs) {
        v *= sqrt(Ekin_needed / Ekin_found);
    }
}

pair<vector<V>, vector<V>> initial_conditions(double L, double K, double T) {
    random_device rd;
    mt19937_64 rng(rd());
    uniform_real_distribution<double> uniform(-1, 1);

    vector<V> r0, v0;

    for (size_t i=0; i<K; i++) {
        for (size_t j=0; j<K; j++) {
            r0.push_back((V() << L * (i + 0.5) / K,
                                 L * (j + 0.5) / K
                         ).finished());
            v0.push_back((V() << uniform(rng),
                                 uniform(rng)
                         ).finished());
        }
    }

    V sum_v = V::Zero();
    for (V &v: v0) {
        sum_v += v;
    }
    for (V &v: v0) {
        v -= sum_v / v0.size();
    }

    scale_velocities(v0, T);

    return make_pair(r0, v0);
}
void work(size_t N, size_t K, double L, double T, double h)
{
    vector<V> r0, v0;
    tie(r0, v0) = initial_conditions(L, K, T);

    auto LennardJones = [L](const V &a, const V &b)
    {
        V r = b - a;
        return 4 * (1 / pow(r.norm(), 12) - 1 / pow(r.norm(), 6));
    };

    auto F = [](const V &a, const V &b)
    {
        const V &r = (b - a);
        return r * 48 * (1 / pow(r.norm(), 14) - 0.5 / pow(r.norm(), 8));
    };

    vector<V> r, v;
    MDSimulator<V> sim(F, LennardJones, r0, v0, L, h);

    ofstream file1("md_T=" + to_string(T) + "_L=" + to_string(L) + ".txt");
    ofstream file2("energy_T=" + to_string(T) + "_L=" + to_string(L) + ".txt");
    ofstream file3("corr_T=" + to_string(T) + "_L=" + to_string(L) + ".txt");

    for (size_t t = 0; t < N; t++) {
        r = sim.getPositions();
        v = sim.getVelocities();
        for (size_t i = 0; i < r.size(); i++) {
            file1 << r[i].transpose() << " " << v[i].transpose() << std::endl;
        }
        file2 << t*h << " " << sim.getKineticEnergy() << " " << sim.getPotentialEnergy() << " " << mean(v).transpose() << std::endl;
        sim.step();
        if (t == 1000)
        {
        	sim.resetCorrelation();
        }
    }

    vector<double> bins = sim.getCorrelation();
    for (double b: bins) {
        file3 << b << " ";
    }
    file3 << std::endl;

    file1.close();
    file2.close();
    file3.close();
}

int main(int argc, char const *argv[])
{
    size_t N = 10000;
    size_t K = 4;
//    double L = 8;
    double h = 1e-3;

   // work(N, K, L, 0.01, h);
  //  work(N*10, K, L, 2, h);

    work(N*10, K, 10, 5, h);
 //   work(N*100, K, 24, 5, h);
    work(N*10, K, 16, 5, h);

}
