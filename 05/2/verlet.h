
#ifndef VERLET_H_XOFLD7T
#define VERLET_H_XOFLD7T

#include <iostream>
#include <vector>
#include <functional>
#include <utility>
#include <iostream>

#include <Eigen/Dense>

using std::vector;
using std::function;
using std::cout;
using std::endl;
using namespace Eigen;

template <typename V>
class MDSimulator
{
    public:
        MDSimulator(function<V(const V&, const V&)> force,
                    function<double(const V&, const V&)> potential,
                    vector<V> r0,
                    vector<V> v0,
                    double length,
                    double stepSize,
                    size_t binCount=1000) :
            r(r0),
            v(v0),
            L(length),
            h(stepSize),
            corrSteps(0),
            corrHist(vector<double>(binCount, 0))
        {
            // Wrap around force
            F = [force, length](const V &a, const V &b) {
                V r = b - a;

                r = wrap(r, length);

                if (r.norm() >= length / 2) {
                    return static_cast<V>(V::Zero());
                } else {
                    return force(a, a + r);
                }
            };

            //Wrap around potential
            Pot = [potential, length](const V &a, const V &b) {
                V r = b - a;

                r = wrap(r, length);

                if (r.norm() >= length / 2) {
                    return 0.0;
                } else {
                    return potential(a, a + r);
                }
            };

            // Set initial acceleration
            size_t N = r0.size();
            a = vector<V>(N, V());
            for (size_t i=0; i<N; i++) {
                a[i].setZero();
                for (size_t j=0; j<N; j++) {
                    if (i != j) {
                        a[i] += F(r0[i], r0[j]);
                    }
                }
            }
            r_h = vector<V>(N, V());
            v_h = vector<V>(N, V());
            a_h = vector<V>(N, V());
        };

        void step();
        vector<V> getPositions();
        vector<V> getVelocities();
        double getKineticEnergy();
        double getPotentialEnergy();
        vector<double> getCorrelation();
        void resetCorrelation();
    private:
        vector<V> r;
        vector<V> v;
        vector<V> a;
        vector<V> r_h;
        vector<V> v_h;
        vector<V> a_h;
        function<V(const V&, const V&)> F;
        function<double(const V&, const V&)> Pot;
        double L;
        double h;
        size_t corrSteps;
        vector<double> corrHist;
        static V wrap(const V &r, double L);
        vector<double> instantCorr();
};


template <typename V>
vector<V> MDSimulator<V>::getPositions() {
    return r;
}

template <typename V>
vector<V> MDSimulator<V>::getVelocities() {
    return v;
}

template <typename V>
void MDSimulator<V>::resetCorrelation()
{
    corrSteps = 0;
    for (double &x: corrHist) {
        x = 0;
    }
}

template <typename V>
vector<double> MDSimulator<V>::getCorrelation() {
    size_t bins = corrHist.size();
    size_t N = r.size();
    V v;
    size_t D = v.size();
    vector<double> result = corrHist;
    for (size_t i=0; i<bins; i++) {
        result[i] *= pow(L, D)
                     / (N * N * (pow((double) i / bins * L + L / bins, D) - pow((double) i / bins * L, D)) * pow(M_PI, D / 2.)) 
                     * tgamma(D / 2. + 1);
    }
    return result;
}

template <typename V>
double MDSimulator<V>::getKineticEnergy()
{
    double sum = 0;
    for (const V &vel: v) {
        sum += 0.5 * vel.squaredNorm();
    }
    return sum;
}

template <typename V>
double MDSimulator<V>::getPotentialEnergy()
{
    double sum = 0;

    for (size_t i=0; i<r.size(); i++) {
        for (size_t j=i+1; j<r.size(); j++) {
            sum += -Pot(r[i], r[j]);
        }
    }

    return sum;
}

template <typename V>
V MDSimulator<V>::wrap(const V &r, double L) {
    return r - L * (r / L + V::Constant(0.5)).unaryExpr([](double x){ return floor(x); });
}

template <typename V>
vector<double> MDSimulator<V>::instantCorr()
{
    size_t binCount = corrHist.size();
    size_t N = r.size();
    vector<double> bins(binCount, 0);

    for (size_t i = 0; i < N; i++)
    {
        for (size_t j = 0; j < i; j++)
        {
            const V &diff = wrap(r[j] - r[i], L);
            double d = diff.norm();
            if (d < 0.5 * L)
            {
                bins[(size_t) floor(binCount * 2 * d / L)]++;
            }
        }
    }
    return bins;
}


template <typename V>
void MDSimulator<V>::step()
{
    size_t N = r.size();

    // Step 1
    for (size_t i=0; i<N; i++) {
        r_h[i] = r[i] + v[i] * h + 0.5 * a[i] * h * h;
        // Wrap position
        r_h[i] -= L * (r_h[i] / L).unaryExpr([](double x){ return floor(x); });
    }

    // Step 2
    for (size_t i=0; i<N; i++) {
        a_h[i].setZero();
        for (size_t j=0; j<N; j++) {
            if (i != j) {
                a_h[i] += F(r_h[i], r_h[j]);
            }
        }
    }

    // Step 3
    for (size_t i=0; i<N; i++) {
        v_h[i] = v[i] + 0.5 * (a[i] + a_h[i]) * h;
    }

    r = r_h;
    v = v_h;
    a = a_h;


    // Online calculation of mean particle distance
    // (http://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Online_algorithm)
    vector<double> bins = instantCorr();
    corrSteps++;
    for (size_t i=0; i < corrHist.size(); i++) {
        corrHist[i] += (bins[i] - corrHist[i]) / corrSteps;
    }
}

#endif /* end of include guard: VERLET_H_XOFLD7T */
