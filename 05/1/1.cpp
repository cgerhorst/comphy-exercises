#include <cmath>
#include <cstdio>
#include <functional>
#include <random>

#include <omp.h>

#include <Eigen/Dense>

using namespace std;
using Eigen::VectorXd;

void forces(function<void(const VectorXd&, VectorXd&)> f, double L, size_t N, VectorXd *r, VectorXd *F)
{
	for (size_t i = 0; i < N; i++)
	{
		F[i] = VectorXd::Zero(r[0].rows());
	}
	for (size_t i = 0; i < N; i++)
	{
		for (size_t j = 0; j < i; j++)
		{
			VectorXd diff = (r[j] - r[i]).unaryExpr([L](double c)
			{
				return c - L * floor(c / L + 0.5);
			});
			if (diff.norm() > L / 2)
			{
				continue;
			}
			VectorXd force;
			f(diff, force);
			F[i] += force;
			F[j] -= force;
		}
	}
}

void MDsimulation(function<void(const VectorXd&, VectorXd&)> f, double L, size_t N, size_t time, VectorXd **r, VectorXd **v, VectorXd **F)
{
	double h = 0.01;
	forces(f, L, N, r[0], F[0]);
	for (size_t t = 1; t < time; t++)
	{
		for (size_t i = 0; i < N; i++)
		{
			r[t][i] = (r[t - 1][i] + h * v[t - 1][i] + 0.5 * h * h * F[t - 1][i]).unaryExpr([L](double c)
			{
				return c - L * floor(c / L);
			});
		}
		forces(f, L, N, r[t], F[t]);
		for (size_t i = 0; i < N; i++)
		{
			v[t][i] = v[t - 1][i] + 0.5 * h * (F[t][i] + F[t - 1][i]);
		}
	}
}

void v_cms(size_t N, VectorXd *v, VectorXd &result)
{
	for (size_t i = 0; i < N; i++)
	{
		result += v[i] / N;
	}
}

double E_pot(double L, size_t N, VectorXd *r)
{
	double sum = 0;
	for (size_t i = 0; i < N; i++)
	{
		for (size_t j = 0; j < i; j++)
		{
			VectorXd diff = (r[j] - r[i]).unaryExpr([L](double c)
			{
				return c - L * floor(c / L + 0.5);
			});
			double d = diff.norm();
			if (d > L / 2)
			{
				continue;
			}
			sum += 4 * (pow(1 / d, 12) - pow(1 / d, 6));
		}
	}
	return sum;
}

double E_kin(size_t N, VectorXd *v)
{
	double sum = 0;
	for (size_t i = 0; i < N; i++)
	{
		sum += v[i].squaredNorm();
	}
	return 0.5 * sum;
}

void g_instant(double L, size_t N, VectorXd *r, size_t bins, VectorXd &p)
{
	for (size_t i = 0; i < N; i++)
	{
		for (size_t j = 0; j < i; j++)
		{
			VectorXd diff = (r[j] - r[i]).unaryExpr([L](double c)
			{
				return c - L * floor(c / L + 0.5);
			});
			double d = diff.norm();
			if (d < L)
			{
				p[(size_t) floor(bins * d / L)]++;
			}
		}
	}
}

void g(double L, size_t D, size_t N, VectorXd **r, size_t bins, size_t t0, size_t t1, VectorXd &result)
{
	// Welford-Algorithm
	size_t n = 0;
	for (size_t t = t0; t < t1; t++)
	{
		VectorXd tmp = VectorXd::Zero(bins);
		g_instant(L, N, r[t], bins, tmp);
		n++;
		result += (tmp - result) / n;
	}
	for (size_t b = 0; b < bins; b++)
	{
		result[b] *= pow(L, D) / (N * N * (pow((double) b / bins * L + L / bins, D) - pow((double) b / bins * L, D)) * pow(M_PI, D / 2.)) * tgamma(D / 2. + 1);
	}
}

void work(double L, double T)
{
	size_t D = 2;
	size_t N = 16;
	size_t time = 2e5;

	auto Lennard_Jones_force = [D](const VectorXd &diff, VectorXd &result)
	{
		double r = diff.norm();
		result = - 48 * (pow(1 / r, 14) - 0.5 * pow(1 / r, 8)) * diff;
	};

	VectorXd **r = new VectorXd*[time];
	VectorXd **v = new VectorXd*[time];
	VectorXd **F = new VectorXd*[time];
	for (size_t t = 0; t < time; t++)
	{
		r[t] = new VectorXd[N];
		v[t] = new VectorXd[N];
		F[t] = new VectorXd[N];
	}

	for (size_t m = 0; m < 4; m++)
	{
		for (size_t n = 0; n < 4; n++)
		{
			r[0][4 * m + n] = VectorXd(D);
			r[0][4 * m + n] << L / 8 * (1 + 2 * m),
			                   L / 8 * (1 + 2 * n);
		}
	}

	default_random_engine generator;
	normal_distribution<double> distribution(0, 1);
	VectorXd vg = VectorXd::Zero(D);
	for (size_t i = 0; i < N; i++)
	{
		v[0][i] = VectorXd(D);
		v[0][i] << distribution(generator),
		           distribution(generator);
		vg += v[0][i] / N;
	}
	for (size_t i = 0; i < N; i++)
	{
		v[0][i] -= vg;
	}
	double E_kin0 = E_kin(N, v[0]);
	for (size_t i = 0; i < N; i++)
	{
		v[0][i] *= sqrt(0.5 * T / E_kin0 * (D * N - D));
	}

	MDsimulation(Lennard_Jones_force, L, N, time, r, v, F);

	char name[256];
	sprintf(name, "pos-%.0f-%.3f.csv", L, T);
	FILE *file = fopen(name, "w");
	for (size_t t = 0; t < time; t += 100)
	{
		for (size_t i = 0; i < N; i++)
		{
			fprintf(file, "%.15f %.15f\n", r[t][i][0], r[t][i][1]);
		}
	}
	fclose(file);

	sprintf(name, "meas-%.0f-%.3f.csv", L, T);
	file = fopen(name, "w");
	for (size_t t = 0; t < time; t++)
	{
		VectorXd vcms = VectorXd::Zero(D);
		v_cms(N, v[t], vcms);
		fprintf(file, "%zu %.15f %.15f %.15f\n", t, vcms.norm(), E_kin(N, v[t]), E_pot(L, N, r[t]));
	}
	fclose(file);

	sprintf(name, "g-%.0f-%.3f.csv", L, T);
	file = fopen(name, "w");
	size_t bins = 1000;
	VectorXd g_result = VectorXd::Zero(bins);
	g(L, D, N, r, bins, 10000, time, g_result);
	for (size_t b = 0; b < bins; b++)
	{
		fprintf(file, "%zu %.15f\n", b, g_result[b]);
	}
	fclose(file);

	for (size_t i = 0; i < time; i++)
	{
		delete[] r[i];
		delete[] v[i];
		delete[] F[i];
	}
	delete[] r;
	delete[] v;
	delete[] F;
}

int main()
{
	Eigen::initParallel();
	#pragma omp parallel sections
	{
		#pragma omp section
		{
			work(8, 3);
		}
		#pragma omp section
		{
			work(8, 0.01);
		}
		#pragma omp section
		{
			work(6, 0.01);
		}
		#pragma omp section
		{
			work(4, 0.01);
		}
	}
}
