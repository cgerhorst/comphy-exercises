import matplotlib.pyplot as plt
import numpy as np

r, g0, g1, g2, g3 = np.loadtxt("2a.txt", unpack=True)

plt.xlim(r[0], r[-1])
plt.plot(r, g0, label="$n=0$")
plt.plot(r, g1, label="$n=1$")
plt.plot(r, g2, label="$n=2$")
plt.plot(r, g3, label="$n=3$")
plt.plot(r, np.zeros(len(r)))
plt.legend(loc="best")

plt.savefig("g.pdf")
