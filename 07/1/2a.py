import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

def g(f, n, x0, r):
    x = x0
    for i in range(2**n):
        x = f(r, x)
    return 0.5 - x

f = lambda r, x: r * x * (1 - x)
r = np.linspace(0, 3.6, 1000)
n = 4
x0 = 0.5

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.axhline(y=0, c='k', ls='--')
for i in range(n):
    ax.plot(r, g(f, i, x0, r), label="$n = {}$".format(i))
ax.set_xlabel("$r$")
ax.set_ylabel("$g_n(r)$")
ax.set_xlim(1, 3.6)
ax.legend(loc='lower left')
ax2 = fig.add_axes((0.4, 0.6, 0.4, 0.4))
ax2.axhline(y=0, c='k', ls='--')
for i in range(n):
    ax2.plot(r, g(f, i, x0, r), label="$n = {}$".format(i))
ax2.set_xlim(3.45, 3.6)
ax2.set_ylim(-0.1, 0.2)
ax2.set_xlabel("$r$")
ax2.set_ylabel("$g_n(r)$")
fig.savefig("2a.pdf")
