import matplotlib as mpl
import matplotlib.pylab as plt
import numpy as np
import sys

x = np.loadtxt(sys.argv[1])

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(x[:,0], x[:, 1:], "b,")
ax.set_xlim(1, np.ceil(x[-1,0]))
fig.savefig(sys.argv[2])
