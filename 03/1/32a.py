import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

def x(b, u):
    return b * u * (u**2 - 3) / (u**2 + 1)

def y(b, u):
    return -b * (u**2 - 3) / (u**2 + 1)

u = np.linspace(-5, 5, 10000)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
for b in range(1, 4):
    ax.plot(x(b, u), y(b, u), label='$b = {}$'.format(b))
ax.set_xlabel('$x(u)$')
ax.set_ylabel('$y(u)$')
ax.legend(loc='best')
fig.savefig('32a.pdf')
