#include <cmath>
#include <functional>

#include "1.h"

using namespace std;

double derive(function<double(double)> f, double x, double h)
{
	return (f(x + h) - f(x - h)) / (2 * h);
}

double derive2(function<double(double)> f, double x, double h)
{
	return (f(x + h) - 2 * f(x) + f(x - h)) / (h * h);
}

function<double(double, double)> f(function<double(double)> x, function<double(double)> y)
{
	return [x, y](double u, double du)
	{
		double h = 1e-7;
		double dx = derive(x, u, h);
		double ddx = derive2(x, u, h);
		double dy = derive(y, u, h);
		double ddy = derive2(y, u, h);

		return -(du * du * (dx * ddx + dy * ddy) + dy) / (dx * dx + dy * dy);
	};
}

void looping(function<double(double)> x, function<double(double)> y, int N, double t0, double t1, double **udu)
{
	auto g = f(x, y);
	rungekutta4([g](double t, double *udu, double *force)
	{
		force[0] = udu[1];
		force[1] = g(udu[0], udu[1]);
	}, 2, N, t0, t1, udu);
}

double x(double u)
{
	return u * (u * u - 3) / (u * u + 1);
}

double y(double u)
{
	return -(u * u - 3) / (u * u + 1);
}

void ex32c()
{
	int N = 1000;
	double t1 = 1.5;
	double **udu = new double*[N + 1];
	for (int i = 0; i <= N; i++)
	{
		udu[i] = new double[2];
	}
	udu[0][0] = -5;
	udu[0][1] = 10;
	looping(x, y, N, 0, t1, udu);

	FILE *file = fopen("32c.csv", "w");
	auto g = f(x, y);
	double h = 1e-7;
	for (int i = 0; i <= N; i++)
	{
		double u = udu[i][0];
		double du = udu[i][1];
		double ddu = g(u, du);
		double dx = derive(x, u, h);
		double ddx = derive2(x, u, h);
		double dy = derive(y, u, h);
		double ddy = derive2(y, u, h);
		double dxdy = sqrt(dx * dx + dy * dy);
		double ax = ddx * du * du + dx * ddu;
		double ay = ddy * du * du + dy * ddu;
		double Nx = ax;
		double Ny = ay + 1;
		double N = sqrt(Nx * Nx + Ny * Ny);
		double phi = acos((Nx * dx + Ny * dy) / (N * dxdy));
		fprintf(file, "%.15f %.15f %.15f %.15f %.15f %.15f %.15f\n", t1 / N * i, u, du, x(u), y(u), N, phi);
	}
	fclose(file);

	for (int i = 0; i <= N; i++)
	{
		delete[] udu[i];
	}
	delete[] udu;
}

bool looped(double du0)
{
	int N = 10000;
	double **udu = new double*[N + 1];
	for (int i = 0; i <= N; i++)
	{
		udu[i] = new double[2];
	}

	double t0 = 0;
	double Dt = 5;
	udu[0][0] = -5;
	udu[0][1] = du0;

	bool result;
	while (true)
	{
		looping(x, y, N, t0, t0 + Dt, udu);
		if (udu[N][0] > 0)
		{
			result = true;
			break;
		}
		else if (udu[0][0] > udu[N][0])
		{
			result = false;
			break;
		}
		t0 += Dt;
		udu[0][0] = udu[N][0];
		udu[0][1] = udu[N][1];
	}

	for (int i = 0; i <= N; i++)
	{
		delete[] udu[i];
	}
	delete[] udu;

	return result;
}

void ex32d()
{
	double low = 0;
	double high = 2.5;
	double mid;
	do
	{
		mid = (high + low) / 2;
		if (looped(mid))
		{
			high = mid;
		}
		else
		{
			low = mid;
		}
	}
	while (fabs(high - low) >= 1e-5);
	printf("numerisch: %.15f\n", mid);
	printf("exakt:     2.42535625036333");
}

void ex32f()
{
	auto x = [](double u) {return cos(u);};
	auto y = [](double u) {return sin(u);};

	int N = 1000;
	double t1 = 1.5;
	double **udu = new double*[N + 1];
	for (int i = 0; i <= N; i++)
	{
		udu[i] = new double[2];
	}
	udu[0][0] = -5;
	udu[0][1] = 10;
	looping(x, y, N, 0, t1, udu);

	FILE *file = fopen("32f.csv", "w");
	auto g = f(x, y);
	double h = 1e-7;
	for (int i = 0; i <= N; i++)
	{
		double u = udu[i][0];
		double du = udu[i][1];
		double ddu = g(u, du);
		double dx = derive(x, u, h);
		double ddx = derive2(x, u, h);
		double dy = derive(y, u, h);
		double ddy = derive2(y, u, h);
		double dxdy = sqrt(dx * dx + dy * dy);
		double ax = ddx * du * du + dx * ddu;
		double ay = ddy * du * du + dy * ddu;
		double Nx = ax;
		double Ny = ay + 1;
		double N = sqrt(Nx * Nx + Ny * Ny);
		double phi = acos((Nx * dx + Ny * dy) / (N * dxdy));
		fprintf(file, "%.15f %.15f %.15f %.15f %.15f %.15f %.15f\n", t1 / N * i, u, du, x(u), y(u), N, phi);
	}
	fclose(file);

	for (int i = 0; i <= N; i++)
	{
		delete[] udu[i];
	}
	delete[] udu;
}

int main()
{
	ex32c();
	ex32d();
	ex32f();
	return 0;
}
