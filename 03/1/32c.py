import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

t, u, du, x, y, N, phi = np.loadtxt('32c.csv', unpack=True)
X = []
Y = []
for i in range(0,len(x), 8):
    X.append(x[i])
    Y.append(y[i])

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(X, Y, 'bo')
ax.set_xlabel('$x(u(t))$')
ax.set_ylabel('$y(u(t))$')
fig.savefig('32c.pdf')

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(t, N, 'b-', label="$N(t)$")
ax.plot(t, phi, 'r-', label="$\phi$")
ax.set_xlabel('$t$')
ax.legend(loc="best")
fig.savefig('32e.pdf')
