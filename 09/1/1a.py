import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

t, mean_x, mean_y, moment2 = np.loadtxt("1a.csv", unpack=True)

fig = plt.figure()
ax1 = fig.add_subplot(2, 1, 1)
ax1.plot(t, mean_x, "r-", label=r"$\langle x \rangle$")
ax1.plot(t, mean_y, "b-", label=r"$\langle y \rangle$")
ax1.legend(loc="best")
plt.setp(ax1.get_xmajorticklabels(), visible=False)

ax2 = fig.add_subplot(2, 1, 2)
ax2.plot(t, moment2, "r-")
ax2.set_xlabel("$t$")
ax2.set_ylabel(r"$\langle r^2 \rangle$")
fig.savefig("1a.pdf")
