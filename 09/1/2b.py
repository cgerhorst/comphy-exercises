import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

N = [10**k for k in range(1, 7)]

error = np.loadtxt("2b.csv", unpack=True)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.plot(N, error, "ro")
ax.set_xlabel("$N$")
ax.set_ylabel(r"$\langle f \rangle - \frac{\pi}{4}$")
ax.set_xscale("log")
ax.set_yscale("log")
fig.savefig("2b.pdf")
