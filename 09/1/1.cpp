#include <functional>
#include <random>
#include <tuple>
#include <vector>

using namespace std;

tuple<vector<vector<int>>, vector<vector<int>>> random_walk(size_t walks, size_t time)
{
	uniform_int_distribution<int> dist(0, 3);
	mt19937 gen;
	function<int()> random = bind(dist, gen);
	vector<vector<int>> x(walks, vector<int>(time, 0));
	vector<vector<int>> y(walks, vector<int>(time, 0));
	for (size_t w = 0; w < walks; w++)
	{
		for (size_t t = 1; t < time; t++)
		{
			x[w][t] = x[w][t - 1];
			y[w][t] = y[w][t - 1];
			switch (random())
			{
				case 0:
					x[w][t]++;
					break;
				case 1:
					y[w][t]++;
					break;
				case 2:
					x[w][t]--;
					break;
				case 3:
					y[w][t]--;
					break;
			}
		}
	}
	return make_tuple(x, y);
}

void a(const vector<vector<int>> &x, const vector<vector<int>> &y)
{
	size_t walks = x.size();
	size_t time = x[0].size();
	FILE *file = fopen("1a.csv", "w");
	for (size_t t = 0; t < time; t++)
	{
		double mean_x = 0;
		double mean_y = 0;
		double moment2 = 0;
		for (size_t w = 0; w < walks; w++)
		{
			mean_x += (double) x[w][t] / walks;
			mean_y += (double) y[w][t] / walks;
			moment2 += (double) (pow(x[w][t], 2) + pow(y[w][t], 2)) / walks;
		}
		fprintf(file, "%zu %.20f %.20f %.20f\n", t, mean_x, mean_y, moment2);
	}
	fclose(file);
}

void c(const vector<vector<int>> &x, const vector<vector<int>> &y, size_t t)
{
	size_t walks = x[0].size();
	vector<vector<double>> array(2 * t + 1, vector<double>(2 * t + 1, 0));
	for (size_t w = 0; w < walks; w++) 
	{
		array[x[w][t] + t][y[w][t] + t] += 1.0 / walks;
	}
	char name[256];
	sprintf(name, "1c-%zu.csv", t);
	FILE *file = fopen(name, "w");
	for (size_t i = 0; i < 2 * t + 1; i++)
	{
		for (size_t j = 0; j  < 2 * t + 1; j++)
		{
			fprintf(file, " %.20f", array[i][j]);
		}
		fprintf(file, "\n");
	}
}

int main()
{
	vector<vector<int>> x;
	vector<vector<int>> y;
	tie(x, y) = random_walk(100000, 1000);
	a(x, y);
	c(x, y, 10);
	c(x, y, 100);
	c(x, y, 998);
	return 0;
}
