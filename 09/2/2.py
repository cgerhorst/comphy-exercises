import matplotlib.pyplot as plt
import numpy as np

err = np.loadtxt("pi_err.txt", unpack=True)

k = np.linspace(1, 6, 6)
plt.xlim(0, 7)
plt.xlabel("$k$")
plt.ylabel(r"$\Delta$")
plt.semilogy(k, err, "x")
plt.savefig("pi_err.pdf")
plt.clf()

areas = np.loadtxt("circ_area.txt", unpack=True)
plt.hist(areas, bins=20)
plt.savefig("circ_area.pdf")
plt.clf()

area_a = np.loadtxt("el_area.txt", unpack=True)
a = b = np.arange(1,20)
plt.plot(a, area_a, 'r.')
plt.xlim(0, 21)
plt.xlabel("$a$")
plt.savefig("el_area.pdf")
