#include <cstdio>
#include <fstream>
#include <functional>
#include <iostream>
#include <random>
#include <stack>
#include <tuple>

#include <omp.h>

#include <Eigen/Dense>

using namespace std;
using Eigen::ArrayXXi;

ArrayXXi generate(int L, double p, function<double()> &random)
{
	return ArrayXXi::Zero(L, L).unaryExpr([&random, p](int x){ return random() < p ? 1 : 0; });
}

int cluster(ArrayXXi &array, ArrayXXi &c, int L, int x, int y, int colour)
{
	if (c(x, y) != 0 || array(x, y) == 0)
	{
		return colour;
	}

	colour++;
	c(x, y) = colour;
	stack<tuple<int, int>> s;
	s.push(make_tuple(x, y));

	while (!s.empty())
	{
		int x, y;
		tie(x, y) = s.top();
		s.pop();

		for (int i = -1; i <= 1; i += 2)
		{
			if (x + i >= 0 && x + i < L)
			{
				if (c(x + i, y) == 0 && array(x + i, y) != 0)
				{
					c(x + i, y) = colour;
					s.push(make_tuple(x + i, y));
				}
			}
			if (y + i >= 0 && y + i < L)
			{
				if (c(x, y + i) == 0 && array(x, y + i) != 0)
				{
					c(x, y + i) = colour;
					s.push(make_tuple(x, y + i));
				}
			}
		}
	}
	return colour;
}

void picture(double p, int L, function<double()> &random)
{
	ArrayXXi array = generate(L, p, random);
	ArrayXXi c = ArrayXXi::Zero(L, L);

	int colour = 1;

	for (int i = 0; i < L; i++)
	{
		for (int j = 0; j < L; j++)
		{
			colour = cluster(array, c, L, i, j, colour);
		}
	}
	
	char name[256];
	sprintf(name, "b-a-%.1f.csv", p);
	ofstream f(name);
	f << array << endl;
	f.close();
	sprintf(name, "b-c-%.1f.csv", p);
	f.open(name);
	f << c << endl;
	f.close();
}

tuple<bool, int> percolate(int L, double p, function<double()> &random)
{
	ArrayXXi array = generate(L, p, random);
	ArrayXXi c = ArrayXXi::Zero(L, L);

	int colour = 1;
	int p_cluster = 0;

	for (int i = 0; i < L; i++)
	{
		int old = colour;
		colour = cluster(array, c, L, i, 0, colour);
		if (p_cluster == 0 && colour != old)
		{
			bool p1, p2, p3;
			p1 = p2 = p3 = false;
			for (int j = 0; j < L; j++)
			{
				if (c(0, j) == colour)
				{
					p1 = true;
					break;
				}
			}
			for (int j = 0; j < L; j++)
			{
				if (c(L - 1, j) == colour)
				{
					p2 = true;
					break;
				}
			}
			for (int j = 0; j < L; j++)
			{
				if (c(j, L - 1) == colour)
				{
					p3 = true;
					break;
				}
			}
			if (p1 && p2 && p3)
			{
				p_cluster = colour;
			}
		}
	}

	for (int i = 0; i < L; i++)
	{
		for (int j = 1; j < L; j++)
		{
			colour = cluster(array, c, L, i, j, colour);
		}
	}

	int *M = new int[colour + 1]();
	for (int i = 0; i < L; i++)
	{
		for (int j = 0; j < L; j++)
		{
			M[c(i, j)]++;
		}
	}

	int M_inf = 0;
	for (int i = 1; i <= colour; i++)
	{
		if (i != p_cluster && M[i] > M_inf)
		{
			M_inf = M[i];
		}
	}

	delete[] M;
	return make_tuple(p_cluster != 0, M_inf);
}

tuple<double, double> probability(int L, double p, int R, function<double()> &random)
{
	double c = 0;
	double M_inf = 0;
	for (int i = 0; i < R; i++)
	{
		bool a;
		int b;
		tie(a, b) = percolate(L, p, random);
		c += a ? 1 : 0;
		M_inf += b;
	}
	return make_tuple(c / R, M_inf / R);
}

void avg(int L, int R, function<double()> random)
{
	char name[256];
	sprintf(name, "c-%d.csv", L);
	FILE *file = fopen(name, "w");
	for (int i = 0; i < 100; i++)
	{
		double p = i / 100.0;
		double q, M_inf;
		tie(q, M_inf) = probability(L, p, R, random);
		fprintf(file, "%.20f %.20f %.20f\n", p, q, M_inf);
	}
	fclose(file);
}

int main()
{
	Eigen::initParallel();

	mt19937 gen;
	uniform_real_distribution<double> dist;
	function<double()> random = bind(dist, gen);

	#pragma omp parallel sections
	{
		#pragma omp section
		{
			picture(0.1, 50, random);
			picture(0.5, 50, random);
			picture(0.9, 50, random);
		}

		#pragma omp section
		{
			avg(10, 1e3, random);
		}
		#pragma omp section
		{
			avg(50, 1e3, random);
		}
		#pragma omp section
		{
			avg(100, 1e3, random);
		}
		#pragma omp section
		{
			avg(1000, 1e1, random);
		}
	}

	return 0;
}
