import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import sys

p = sys.argv[1]

a = np.loadtxt("b-a-{}.csv".format(p))
c = np.loadtxt("b-c-{}.csv".format(p))

fig = plt.figure()
ax1 = fig.add_subplot(1, 2, 1)
ax1.matshow(a)

ax2 = fig.add_subplot(1, 2, 2)
ax2.matshow(c)

fig.savefig("b-{}.pdf".format(p))
