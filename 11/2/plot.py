
import matplotlib
from pylab import *
from scipy.optimize import curve_fit
from scipy.optimize import brentq
from itertools import combinations

for fname in ['array_50_0.100000_.txt',
              'array_50_0.500000_.txt',
              'array_50_0.900000_.txt',
             ]:
    m = loadtxt(fname)
    #cmap = matplotlib.colors.ListedColormap(rand( 256,3))
    matshow(m)
    savefig(fname.replace('.txt', '.pdf'))
    clf()


f = lambda x, a, b, c, d: a * tanh(b * x + c) + d
fs = []

for fname, color, label in [('hist_10.txt', 'red', '$L=10$'),
                            ('hist_25.txt', 'blue', '$L=25$'),
                            ('hist_50.txt', 'green', '$L=50$'),
                            ('hist_75.txt', 'grey', '$L=75$'),
                            ('hist_100.txt', 'black', '$L=100$'),
                            ('hist_200.txt', 'gold', '$L=200$'),
                            ]:
    h = loadtxt(fname)/500
    x = linspace(0, 1, len(h))
    xs = linspace(0, 1, 1000)
    p0 = [0.5, 1, -0.6, 0.6]
    params, pcov = curve_fit(f, x, h, p0=p0)
    fs.append(lambda x, params=params: f(x, *params))

    plot(xs, f(xs, *params), color=color, linestyle='-', label=label)
    plot(x, h, linestyle='', color=color, marker='+')

ylabel('$q_L(p)$', rotation='horizontal')
xlabel('$p$')
legend(loc='best')
tight_layout()
savefig('hist.pdf')
clf()

p_c = []
for f1, f2 in combinations(fs, 2):
    # Calculate intersections of all combinations f1 and f2
    p_c.append(brentq(lambda x: f1(x) - f2(x), 0.55, 0.65))

print('p_c mean: ', mean(p_c), '+-', std(p_c))

# Possible output:
# p_c mean: 0.600073676852 +- 0.00184904179976

h = loadtxt('hist_beta.txt')
plot(h)
ylabel('$M_\\infty(p)$')
xlabel('$p$')
tight_layout()
savefig('hist_beta.pdf')
clf()


